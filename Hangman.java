import java.util.Scanner;
public class Hangman{

	public static int isLetterInWord(String word, char c) {
	for (int i =0; i<4 ; i++){
	
		if (word.charAt(i)== toUpperCase(c)){
			return i;
		}
			
	}
	   return -1;
}
	public static char toUpperCase(char c) {
		return Character.toUpperCase(c);
	
	}
	public static void printWork(String word, boolean letter[]) {
			String display = "";
		for(int i = 0; i < 4; i++){
			if(letter[i] == true){
				display= display + word.charAt(i);
			}else{
				display = display + "_ ";
			}
		} 
		
		System.out.println(display);
	}
	public static  void runGame(String word){
		Scanner reader = new Scanner(System.in);
		boolean[] letters = new boolean[]{false,false,false,false};
		int i=0;
		while(i < 6){
			System.out.println("Gimme your guess");
			char c = reader.next().charAt(0);
			int charCheck = isLetterInWord(word, c);
		
			if(charCheck == -1 ){
				i++;
				System.out.println("you suck try again");
			}else{
				letters[charCheck] = true;
				System.out.println("Nice you got it");
			}
			printWork(word, letters);
			
			if(i == 6){
				System.out.println("You lose nice try!!");
			}
			
			if(letters[0] == true && letters[1] == true && letters[2] == true && letters[3] == true){
				i=7;
				System.out.println("Yay you win... you have no life");
			}
			
		}
	}
	public static void main(String[] args){
		Scanner reader = new Scanner (System.in);
		//Get user input
		System.out.println("Enter a 4-letter word:");
		String word = reader.next();
		//Convert to upper case
		word = word.toUpperCase();
		
		//Start hangman game
	    runGame(word);	
	}
}
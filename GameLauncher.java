import java.util.Scanner;
public class GameLauncher{
	public static void main(String[] args){
		gameRun();
	}
	public static void choseGame(String choice){
		//if user selects 1 runs wordle
		if(choice.equals("1")){
			String answer = Wordle.generateWord();
			//runs wordle game
			Wordle.runGame(answer);
		}
		//if user selects 2 runs hangman
		if(choice.equals("2")){
			Scanner reader = new Scanner (System.in);
			//Get user input
			System.out.println("Enter a 4-letter word:");
			String word = reader.next();
			//Convert to upper case
			word = word.toUpperCase();
			//Start hangman game
			Hangman.runGame(word);	
		}
		//if neither 1 or 2 is selected runs gameRun again
		if(!(choice.equals("1") && choice.equals("2"))){
			System.out.println("Try again!!");
			gameRun();
		}
	}
	public static void gameRun(){
		Scanner reader = new Scanner(System.in);
		//takes user input
		System.out.println("Welcome to the Game Launcher press 1 to play Wordle or 2 to play Hangman");
		String choice = reader.nextLine();
		choseGame(choice);
	}
}
	
		
		

	